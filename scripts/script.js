"use strict";

// 1. HTML тег на сторінці можна створити за допомогою команди - document.createElement("tag"). 
// 2. Перший параметр означає куди потрібно вставити відносно елементу, 
//     вставлти ми можемо до відкриваючого тега, після відкриваючого тега, до першого нащадка або до останнього нащадка.
// 3. Найпростіший спосіб видалити елемент це метод remove(). 


let arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
function createList(arr, parent = document.body) {
let list = document.createElement("ul");
parent.append(list);
    arr.forEach(el => {
        let li = document.createElement('li');
        li.textContent = el;
        list.append(li);
    });
}
createList(arr);
